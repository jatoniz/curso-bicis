const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const usuario = require('../models/usuario');

passport.use(new localStrategy(
    function(email, password, done) {
        usuario.findOne({ email: email }, function(err, user) {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            if (!user.validPassword(password)) { return done(null, false); }
            return done(null, user);
        });
    }
));

passport.use(new GoogleStrategy({
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callBackURL: process.env.HOST + '/auth/google/callback'
    },
    function(accesToken, refreshToken, profile, cb) {
        console.log(profile);
        usuario.findOneOrCreateByGoogle(profile, function(err, user) {
            return cb(err, user);
        });
    }
));

passport.serializeUser(function(user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
    usuario.findById(id, function(err, usuario) {
        cb(err, usuario);
    });
});

module.exports = passport;