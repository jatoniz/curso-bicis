var mongoose = require('mongoose');
var schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var uniqueValidator = require('mongoose-unique-validator');
var Token = require('../models/token');
var crypto = require('crypto');
var mailer = require('../mailer/mailer');

const saltRounds = 10;

const validateEmail = function(email) {
    const re = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
    return re.test(email);
}

const usuarioSchema = new schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        validate: [validateEmail, 'Por favor, ingrese un email valido.'],
        match: [/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/]
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });

usuarioSchema.pre('save', function(next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.findOneOrCreateByGoogle = function(profile, cb) {
    return (cb(profile));
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destino = this.email;
    token.save(function(err) {
        if (err) { return console.log(err.message); }

        const emailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destino,
            subject: 'Verificación de cuenta',
            text: 'Hola, por favor para verificar su cuenta haga click en el siguiente enlance:' + 'http://localhost:5000' + '\/token/confirmation\/' + token.token
        };

        mailer.sendMail(emailOptions, function(err) {
            if (err) { return console.log(err.message); }
            console.log('A verification email has benn sent to: ' + email_destino);
        });
    });
}

module.exports = mongoose.model('Usuario', usuarioSchema);