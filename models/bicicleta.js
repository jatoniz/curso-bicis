var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;

var BicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: { type: '2dsphere', sparse: true }
    }
});

BicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

BicicletaSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | Color: ' + this.color;
};

BicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb);
};

BicicletaSchema.statics.add = function(bici, cb) {
    return this.create(bici, cb);
};

BicicletaSchema.statics.findByCode = function(code, cb) {
    return this.findOne({ code: code }, cb);
};

BicicletaSchema.statics.removeByCode = function(code, cb) {
    return this.deleteOne(code, cb);
};

module.exports = Mongoose.model('Bicicleta', BicicletaSchema);